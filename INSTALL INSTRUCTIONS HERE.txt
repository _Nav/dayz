Rewritten for 1.0 //REEEEEE

==============================================================================================================================
1. Extract DayZ folder to C:\

2. Edit serverDZ.cfg CHANGE ME to your settings

3. Edit BEServer_x64 CHANGE ME to your RCON password

4. Allow access in WINDOWS FIREWALL;
   Go to control panel
   Click Windows Firewall
   ADVANCED SETTINGS
   CLICK INBOUND >> PROGRAM >> CLICK BROWSE >> SET PATH TO SERVER EXE or copy >> C:\DayZ\DayZServer_x64.exe
   Repeat for Outbound.

4a. OPEN PORT 2302 AND RANGE 27000-27030 in Router or server control panel.

5. Scheduler.XML needs to be edited, go to C:\DayZ\Battleye\Config\sheduler.xml and edit the messages.
   Lines; 7, 187, 194, 201, 208 are the only ones that need to be edited.
   Change
       <cmd>say -1 CHANGE ME >> C:\DayZ\Battleye\Config\sheduler.xml</cmd>
   To your messages;
       <cmd>say -1 This is my server join my discord blah blah</cmd>

6. Double click STARTDAYZSERVER.bat server will start and your players can join!
==============================================================================================================================

>>Please NOTE!
The server will restart itself after 6 hours. All settings are default. If the server crashes, it will restart itself.

There are two scheduler.xml in C:\DayZ\Battleye\Config. One has messages you can configure. The other is just restart messages and doesnt need to be configured.
To change them simply delete the scheduler.xml from C:\DayZ\Battleye\Config\ and replace it with the one you want from the folders in C:\DayZ\Battleye\Config\


NEED HELP? Go here; https://discord.me/UK420DAYZ
