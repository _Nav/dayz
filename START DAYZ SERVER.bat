::STARTDAYZSERVER.BAT by TermNCR from UK420DAYZ :)
@echo off

::BEC PATH
set BECPath="C:\DayZ\BattlEye"
cls

echo Protecting Server: [ DAYZ SERVER ] from crashes...
title DAYZ SERVER
timeout /T 5

:START
echo Starting BEC
start /D %BECPath% bec.exe --dsc -f Config.cfg
TIMEOUT /T 5
echo Launching Server
C: 
cd "C:\DayZ"
echo TermNCRs DayZ Monitor... Active !
start "Dayz" /wait DayZServer_x64.exe -port=2302 -config=serverDZ.cfg -profiles=C:\DayZ -freezecheck
ping 127.0.0.1 -n 15 >NUL
echo DayZ Shutdown ... Restarting!
ping 127.0.0.1 -n 5 >NUL
cls
goto START